# multiAgents.py
# --------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


from util import manhattanDistance
from game import Directions
import random, util
import math
from itertools import cycle

from game import Agent

class ReflexAgent(Agent):
    """
    A reflex agent chooses an action at each choice point by examining
    its alternatives via a state evaluation function.

    The code below is provided as a guide.  You are welcome to change
    it in any way you see fit, so long as you don't touch our method
    headers.
    """


    def getAction(self, gameState):
        """
        You do not need to change this method, but you're welcome to.

        getAction chooses among the best options according to the evaluation function.

        Just like in the previous project, getAction takes a GameState and returns
        some Directions.X for some X in the set {NORTH, SOUTH, WEST, EAST, STOP}
        """
        # Collect legal moves and successor states
        legalMoves = gameState.getLegalActions()
        
        # Define Pacman position
        self.Pacman_pos = gameState.getPacmanPosition()

        # Choose one of the best actions
        scores = [self.evaluationFunction(gameState, action) for action in legalMoves]
        #print( legalMoves )
        #print( scores )
        bestScore = max(scores)
        bestIndices = [index for index in range(len(scores)) if scores[index] == bestScore]
        chosenIndex = random.choice(bestIndices) # Pick randomly among the best

        "Add more of your code here if you want to"

        return legalMoves[chosenIndex]

    def evaluationFunction(self, currentGameState, action):
        """
        Design a better evaluation function here.

        The evaluation function takes in the current and proposed successor
        GameStates (pacman.py) and returns a number, where higher numbers are better.

        The code below extracts some useful information from the state, like the
        remaining food (newFood) and Pacman position after moving (newPos).
        newScaredTimes holds the number of moves that each ghost will remain
        scared because of Pacman having eaten a power pellet.

        Print out these variables to see what you're getting, then combine them
        to create a masterful evaluation function.
        """
        # Useful information you can extract from a GameState (pacman.py)
        successorGameState = currentGameState.generatePacmanSuccessor(action)
        newPos = successorGameState.getPacmanPosition()
        newFood = successorGameState.getFood()
        newGhostStates = successorGameState.getGhostStates()
        newScaredTimes = [ghostState.scaredTimer for ghostState in newGhostStates]

        "*** YOUR CODE HERE ***"

        # function for boolean into sign
        def signBool( Boolean ):
            if Boolean == True:
                return -1 
            else:
                return 1
        
        # Count food pellets
        food_pellets = 0
        for row in newFood:
            food_pellets += sum( [ 1 for elem in row if elem == True ] )

        # Check which ghosts can be eaten
        Ghost_bool = list( map( lambda x: signBool( x > 2 ), newScaredTimes ) )

        # Define manhattan distance
        def manhattanDistance( location1, location2 ):
            distance = sum( map( lambda x, y: abs( x - y ), location1, location2 ) )
            return distance

        # Define weights
        alpha = [ 0.1, 10, 0, 3 ]
        
        # Measure distance to food
        food_positions = []
        for row_index, row in enumerate( newFood ):
            food_positions += [ ( row_index, col_index ) for col_index, elem in enumerate( row ) if elem == True ]
        
        # Assign score to proximity
        food_proximity_score = sum( [ 20/( 1 + manhattanDistance( newPos, foodPosition ) )**2 for foodPosition in food_positions ] )

        # Capsule proximity
        capsule_proximity_score = sum( [ 50/( manhattanDistance( newPos, capsulePosition ) ) for capsulePosition in successorGameState.getCapsules() ] )
        
        # Then measure distance to ghosts and same principle
        ghost_proximity_score = int( sum( [ Ghost_bool[index] * -30/(1 + manhattanDistance( newPos, ghostPos.getPosition() )**2 ) for index, ghostPos in enumerate( newGhostStates ) ] ) )
        
        # Cost of stopping
        if action == 'Stop':
            alpha[ 2 ] = -5000
        
        totalScore = successorGameState.getScore() + alpha[0]*food_proximity_score + alpha[1]*ghost_proximity_score + 1*alpha[2] + alpha[3] * capsule_proximity_score

        return totalScore


def scoreEvaluationFunction(currentGameState):
    """
    This default evaluation function just returns the score of the state.
    The score is the same one displayed in the Pacman GUI.

    This evaluation function is meant for use with adversarial search agents
    (not reflex agents).
    """
    return currentGameState.getScore()

class MultiAgentSearchAgent(Agent):
    """
    This class provides some common elements to all of your
    multi-agent searchers.  Any methods defined here will be available
    to the MinimaxPacmanAgent, AlphaBetaPacmanAgent & ExpectimaxPacmanAgent.

    You *do not* need to make any changes here, but you can if you want to
    add functionality to all your adversarial search agents.  Please do not
    remove anything, however.

    Note: this is an abstract class: one that should not be instantiated.  It's
    only partially specified, and designed to be extended.  Agent (game.py)
    is another abstract class.
    """

    def __init__(self, evalFn = 'scoreEvaluationFunction', depth = '2'):
        self.index = 0 # Pacman is always agent index 0
        self.evaluationFunction = util.lookup(evalFn, globals())
        self.depth = int(depth)

class MinimaxAgent(MultiAgentSearchAgent):
    """
    Your minimax agent (question 2)
    """

    def getAction(self, gameState):
        """
        Returns the minimax action from the current gameState using self.depth
        and self.evaluationFunction.

        Here are some method calls that might be useful when implementing minimax.

        gameState.getLegalActions(agentIndex):
        Returns a list of legal actions for an agent
        agentIndex=0 means Pacman, ghosts are >= 1

        gameState.generateSuccessor(agentIndex, action):
        Returns the successor game state after an agent takes an action

        gameState.getNumAgents():
        Returns the total number of agents in the game

        gameState.isWin():
        Returns whether or not the game state is a winning state

        gameState.isLose():
        Returns whether or not the game state is a losing state
        """
        "*** YOUR CODE HERE ***"
        
        # Define higher order function minmax_value
        def value( state, depth = 0, agentIndex = 0 ):
            
            # Check if terminal state
            if state.isWin() or state.isLose() or depth == self.depth:
                v = self.evaluationFunction( state )
                return v
            
            # Define number of agents in game
            numAgents = gameState.getNumAgents()
            
            # Change to next agent and select function accordingly 
            agentIndex += 1
            if agentIndex >= numAgents:
                agentIndex = 0
                minMaxFunc = max
            else:
                minMaxFunc = min
            
            # If finished adversarial movements, add 1 to depth
            if agentIndex == numAgents - 1:
                depth += 1
            
            # Define seed
            v = -minMaxFunc( [ float( '-inf' ), float( 'inf' ) ] )
            
            # Get Legal Actions
            LegalActions = state.getLegalActions( agentIndex )
            
            # Get Successor States
            successorStates = [ state.generateSuccessor( agentIndex, action ) for action in LegalActions ]   
            
            # Run through successor states
            for successorState in successorStates:
                v = minMaxFunc( v, value( successorState, depth, agentIndex ) )
            return v
        
        ################### Root value function ################################

        # Define number of agents
        numAgents = gameState.getNumAgents()
        PacmanIndex = 0
        
        # Execute code
        LegalActions = gameState.getLegalActions( PacmanIndex )
        successorStates = [ gameState.generateSuccessor( PacmanIndex, action ) for action in LegalActions ]
        
        # Get values
        stateValues = [ value( state ) for state in successorStates ]
        BestValueIndex = stateValues.index( max( stateValues ) )
        
        # Choose action
        action = LegalActions[ BestValueIndex ]
        
        return action
        
        util.raiseNotDefined()

class AlphaBetaAgent(MultiAgentSearchAgent):
    """
    Your minimax agent with alpha-beta pruning (question 3)
    """

    def getAction(self, gameState):
        """
        Returns the minimax action using self.depth and self.evaluationFunction
        """
        "*** YOUR CODE HERE ***"
        # Define function that provides state value 
        def value( state, alpha = float( '-inf' ), beta = float( 'inf' ), depth = 0, agentIndex = 0 ):

            ############ Check if terminal state ###############
            if state.isWin() or state.isLose() or depth == self.depth:
                v = self.evaluationFunction( state )
                return v
            
            ############## Choose Agent #########################

            ## Define number of agents in game
            numAgents = gameState.getNumAgents()
            
            # Change to next agent and select function accordingly 
            agentIndex += 1
            if agentIndex >= numAgents:
                agentIndex = 0
                minMaxFunc = max
            else:
                minMaxFunc = min
            
            # If finished adversarial movements, add 1 to depth
            if agentIndex == numAgents - 1:
                depth += 1
            
            
            ############### Start seed ##########################
            # Define seed
            v = -minMaxFunc( [ float( '-inf' ), float( 'inf' ) ] )
            
            # Get Legal Actions
            LegalActions = state.getLegalActions( agentIndex )
            
            # Run through successor states
            for action in LegalActions:
                
                # Load successor state
                successorState = state.generateSuccessor( agentIndex, action )
                
                # Update v
                v = minMaxFunc( v, value( successorState, alpha, beta, depth, agentIndex ) )

                # Determine behaviour for different functions
                if minMaxFunc == max:
                    if v > beta: 
                        return v
                    else: 
                        alpha = max( alpha, v )
                
                elif minMaxFunc == min:
                    if v < alpha: 
                        return v
                    else: 
                        beta = min( beta, v )
            
            # Return utility        
            return v
        

        ############## Main #######################

        # Define number of agents
        numAgents = gameState.getNumAgents()
        
        # Define index of Pacman
        PacmanIndex = 0

        # Execute code
        LegalActions = gameState.getLegalActions( PacmanIndex )
        
        # Get values
        alpha, beta, stateValues = float( '-inf' ), float( 'inf' ), []
        for action in LegalActions:
            
            # Generate successor state
            state = gameState.generateSuccessor( PacmanIndex, action )
            
            # Obtain value
            v = value( state, alpha, beta )
            stateValues.append( v )
            if v > beta: 
                return v
            else: 
                alpha = max( alpha, v )
        
        # Get index of successor with best value
        BestValueIndex = stateValues.index( max( stateValues ) )
        
        # Choose action
        action = LegalActions[ BestValueIndex ]
        
        return action
        
        util.raiseNotDefined()

class ExpectimaxAgent(MultiAgentSearchAgent):
    """
      Your expectimax agent (question 4)
    """

    def getAction(self, gameState):
        """
        Returns the expectimax action using self.depth and self.evaluationFunction

        All ghosts should be modeled as choosing uniformly at random from their
        legal moves.
        """
        "*** YOUR CODE HERE ***"
        # Define expectation function for uniform distribution
        def expectation( lst ):
            expectationValue = sum( lst )/len( lst )
            return expectationValue
        
        def value( state, depth = 0, agentIndex = 0 ):
            
            # Check if terminal state
            if state.isWin() or state.isLose() or depth == self.depth:
                v = self.evaluationFunction( state )
                return v
            
            # Define number of agents in game
            numAgents = gameState.getNumAgents()
            
            # Change to next agent and select function accordingly 
            agentIndex += 1
            
            # Decide type of node
            if agentIndex >= numAgents:
                agentIndex = 0
                minMaxFunc = max
            else:
                minMaxFunc = expectation
            
            # Get Legal Actions
            LegalActions = state.getLegalActions( agentIndex )
            
            # If finished adversarial movements, add 1 to depth
            if agentIndex == numAgents - 1:
                depth += 1
            
            # Get values of each successor state
            stateValues = [ value( state.generateSuccessor( agentIndex, action ), depth, agentIndex ) for action in LegalActions ] 
            
            # Apply corresponding function
            v = minMaxFunc( stateValues )
            return v
        
        ################### Root value function ################################

        # Define number of agents
        numAgents = gameState.getNumAgents()
        PacmanIndex = 0
        
        # Execute code
        LegalActions = gameState.getLegalActions( PacmanIndex )
        successorStates = ( gameState.generateSuccessor( PacmanIndex, action ) for action in LegalActions )
        
        # Get values
        stateValues = [ value( state ) for state in successorStates ]
        BestValueIndex = stateValues.index( max( stateValues ) )
        
        # Choose action
        action = LegalActions[ BestValueIndex ]
        
        return action

        util.raiseNotDefined()


def betterEvaluationFunction(currentGameState):
    """
    Your extreme ghost-hunting, pellet-nabbing, food-gobbling, unstoppable
    evaluation function (question 5).

    DESCRIPTION: <write something here so we know what you did>
    """
    "*** YOUR CODE HERE ***"

    newPos = currentGameState.getPacmanPosition()
    
    newFood = currentGameState.getFood()
    
    newGhostStates = currentGameState.getGhostStates()
    newScaredTimes = [ghostState.scaredTimer for ghostState in newGhostStates]

    "*** YOUR CODE HERE ***"

    # function for boolean into sign
    def signBool( Boolean ):
        if Boolean == True:
            return -1 
        else:
            return 1
    
    # Count food pellets
    food_pellets = 0
    for row in newFood:
        food_pellets += sum( [ 1 for elem in row if elem == True ] )

    # Check which ghosts can be eaten
    Ghost_bool = list( map( lambda x: signBool( x > 2 ), newScaredTimes ) )

    # Define manhattan distance
    def manhattanDistance( location1, location2 ):
        distance = sum( map( lambda x, y: abs( x - y ), location1, location2 ) )
        return distance

    # Define weights
    alpha = [ 0.1, 20, 0, 3 ]
    
    # Measure distance to food
    food_positions = []
    for row_index, row in enumerate( newFood ):
        food_positions += [ ( row_index, col_index ) for col_index, elem in enumerate( row ) if elem == True ]
    
    # Assign score to proximity
    food_proximity_score = sum( [ 30/( 1 + manhattanDistance( newPos, foodPosition ) )**2 for foodPosition in food_positions ] )

    # Capsule proximity
    capsule_proximity_score = sum( [ 50/( manhattanDistance( newPos, capsulePosition ) ) for capsulePosition in currentGameState.getCapsules() ] )
    
    # Then measure distance to ghosts and same principle
    ghost_proximity_score = int( sum( [ Ghost_bool[index] * -30/(1 + manhattanDistance( newPos, ghostPos.getPosition() )**2 ) for index, ghostPos in enumerate( newGhostStates ) ] ) )
    
    # The final score is defined with a simple linear function weighed on the scores defined above
    totalScore = currentGameState.getScore() + alpha[0]*food_proximity_score + alpha[1]*ghost_proximity_score + 1*alpha[2] + alpha[3] * capsule_proximity_score

    return totalScore

    util.raiseNotDefined()

# Abbreviation
better = betterEvaluationFunction
