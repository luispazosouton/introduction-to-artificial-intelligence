# Introduction to Artificial Intelligence

This repository shows some of the code that I wrote for the course on Introduction to Artificial Intelligence (CS188) from UC Berkeley. It consists of two projects in which a PACMAN agent uses an increasingly sophisticated AI to beat the ghosts!

- Project 1: 

In this project, I designed an agent for the classic version of Pacman. I implement both minimax and expectimax search and played around with evaluation function design. 

Most of the code files were provided by the instructor, my own code can be found on multiAgents.py.


- Project 2: 

In this project, I implemented value iteration and Q-learning. This was applied for a Pacman agent who learned how to beat the ghosts, and for a simulated crawler robot which learned to walk via reinforcement learning.

I only modified the files: valueIterationAgents.py and qlearningAgents.py.

My contributions can be found on the files right under where it says: ""*** YOUR CODE HERE ***""