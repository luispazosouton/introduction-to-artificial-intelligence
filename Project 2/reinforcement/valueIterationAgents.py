# valueIterationAgents.py
# -----------------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


# valueIterationAgents.py
# -----------------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


import mdp, util

from learningAgents import ValueEstimationAgent
import collections

class ValueIterationAgent(ValueEstimationAgent):
    """
        * Please read learningAgents.py before reading this.*

        A ValueIterationAgent takes a Markov decision process
        (see mdp.py) on initialization and runs value iteration
        for a given number of iterations using the supplied
        discount factor.
    """
    def __init__(self, mdp, discount = 0.9, iterations = 100):
        """
          Your value iteration agent should take an mdp on
          construction, run the indicated number of iterations
          and then act according to the resulting policy.

          Some useful mdp methods you will use:
              mdp.getStates()
              mdp.getPossibleActions(state)
              mdp.getTransitionStatesAndProbs(state, action)
              mdp.getReward(state, action, nextState)
              mdp.isTerminal(state)
        """
        self.mdp = mdp
        self.discount = discount
        self.iterations = iterations
        self.values = util.Counter() # A Counter is a dict with default 0
        self.runValueIteration()

    def runValueIteration(self):
        # Write value iteration code here
        "*** YOUR CODE HERE ***"
        def getQvaluesFromState( state ):
            
            # Define possible actions
            possibleActions = self.mdp.getPossibleActions( state )
            
            # Create list of Qvalues
            Qvalues = [ self.computeQValueFromValues( state, action ) for action in possibleActions ]
            
            return max( Qvalues )
        
        ##### Main ####
        # Define startState
        states = self.mdp.getStates()
        
        # Initialize values
        self.values = { state: 0 for state in states }
        
        depth = 1
        while depth <= self.iterations:

            # Loop over states
            self.values = { state: getQvaluesFromState( state ) for state in states if not self.mdp.isTerminal( state ) }
            
            # Add terminal state
            self.values[ 'TERMINAL_STATE' ] = 0

            # Add 1 to depth
            depth += 1
    
    def getValue(self, state):
        """
          Return the value of the state (computed in __init__).
        """
        return self.values[state]


    def computeQValueFromValues(self, state, action):
        """
          Compute the Q-value of action in state from the
          value function stored in self.values.
        """
        "*** YOUR CODE HERE ***"
        
        # Transition probabilities
        transitionProbs = self.mdp.getTransitionStatesAndProbs( state, action )
        
        # Get Q Value
        expectedValue = 0
        for elem in transitionProbs:
            nextState = elem[ 0 ]
            probability = elem[ 1 ]
            expectedValue += ( self.mdp.getReward( state, action, nextState ) + self.getValue( nextState ) * self.discount ) * probability
        
        return expectedValue 


        
        util.raiseNotDefined()

    def computeActionFromValues(self, state):
        """
          The policy is the best action in the given state
          according to the values currently stored in self.values.

          You may break ties any way you see fit.  Note that if
          there are no legal actions, which is the case at the
          terminal state, you should return None.
        """
        "*** YOUR CODE HERE ***"
        # If state is terminal state, return None
        if self.mdp.isTerminal( state ):
            return None
        
        # Get actions from given state
        possibleActions = self.mdp.getPossibleActions( state )

        # Loop through actions
        QValue, Action = float('-inf'), None
        for action in possibleActions:
            QValueNew = self.computeQValueFromValues( state, action )
            
            # If better
            if QValueNew > QValue:
                QValue, Action = QValueNew, action
        
        return Action 
        
        util.raiseNotDefined()

    def getPolicy(self, state):
        return self.computeActionFromValues(state)

    def getAction(self, state):
        "Returns the policy at the state (no exploration)."
        return self.computeActionFromValues(state)

    def getQValue(self, state, action):
        return self.computeQValueFromValues(state, action)

class AsynchronousValueIterationAgent(ValueIterationAgent):
    """
        * Please read learningAgents.py before reading this.*

        An AsynchronousValueIterationAgent takes a Markov decision process
        (see mdp.py) on initialization and runs cyclic value iteration
        for a given number of iterations using the supplied
        discount factor.
    """
    def __init__(self, mdp, discount = 0.9, iterations = 1000):
        """
          Your cyclic value iteration agent should take an mdp on
          construction, run the indicated number of iterations,
          and then act according to the resulting policy. Each iteration
          updates the value of only one state, which cycles through
          the states list. If the chosen state is terminal, nothing
          happens in that iteration.

          Some useful mdp methods you will use:
              mdp.getStates()
              mdp.getPossibleActions(state)
              mdp.getTransitionStatesAndProbs(state, action)
              mdp.getReward(state)
              mdp.isTerminal(state)
        """
        ValueIterationAgent.__init__(self, mdp, discount, iterations)

    def runValueIteration(self):
        "*** YOUR CODE HERE ***"

        def getValueFromState( state ):
            
            # Define possible actions
            possibleActions = self.mdp.getPossibleActions( state )
            
            # Create list of Qvalues
            Qvalues = [ self.computeQValueFromValues( state, action ) for action in possibleActions ]
            
            return max( Qvalues )
        
        ##### Main ####
        # Define startState
        states = self.mdp.getStates()
        
        # Loop through states
        counter, index = 0, 0
        while counter < self.iterations:
            
            # Define state to update
            state = states[ index ]
            
            # If terminal
            if not self.mdp.isTerminal( state ):

                # Loop over states
                self.values[ state ] = getValueFromState( state )

            # Add 1 to counter and index 
            counter += 1; index += 1

            # If reached limit start, over
            if index == len( states ): index = 0


class PrioritizedSweepingValueIterationAgent(AsynchronousValueIterationAgent):
    """
        * Please read learningAgents.py before reading this.*

        A PrioritizedSweepingValueIterationAgent takes a Markov decision process
        (see mdp.py) on initialization and runs prioritized sweeping value iteration
        for a given number of iterations using the supplied parameters.
    """
    def __init__(self, mdp, discount = 0.9, iterations = 100, theta = 1e-5):
        """
          Your prioritized sweeping value iteration agent should take an mdp on
          construction, run the indicated number of iterations,
          and then act according to the resulting policy.
        """
        self.theta = theta
        ValueIterationAgent.__init__(self, mdp, discount, iterations)

    def runValueIteration(self):
        "*** YOUR CODE HERE ***"
        
        # Define function to get a state's value
        def getValueFromState( state ):
            
            # Define possible actions
            possibleActions = iter( self.mdp.getPossibleActions( state ) )
            
            # Find maximum Q-Value
            QValue = float( '-inf' )
            for action in possibleActions:
                QValue = max( QValue, self.computeQValueFromValues( state, action ) )
            
            return QValue
        


        ################ Main ########################

        # Define states
        states = self.mdp.getStates()
        
        ##### Compute predecessor states for each state ######
        
        # Initialize dictionary
        statePredecessors = { state: set() for state in states }
        
        # Run through states
        for state in states:
            possibleActions = self.mdp.getPossibleActions( state )

            # Consider all actions from each state
            for action in possibleActions: 
                transitionProbs = self.mdp.getTransitionStatesAndProbs( state, action )
                
                # Find reachable states
                for elem in transitionProbs:
                    nextState = elem[ 0 ]; probability = elem[ 1 ]
                    
                    if probability > 0:
                        statePredecessors[ nextState ].add( state )
        

        
        ############ Start update of values ##############
        
        # Initialize priority queue
        priorityQueue = util.PriorityQueue()

        # Run through states
        for state in states:

            # If not terminal
            if not self.mdp.isTerminal( state ):
                
                # Get difference between accurate and old value
                diff = abs( self.values[ state ] - getValueFromState( state ) )
                
                # Add error into queue
                priorityQueue.update( state, -diff )
        
        # Loop through states
        for iteration in range( self.iterations ):
            
            # Check if priority queue is empty
            if priorityQueue.isEmpty():
                break
            
            # Define state to update
            state = priorityQueue.pop()
            
            # If not terminal update the state value
            if not self.mdp.isTerminal( state ):

                # Loop over states
                self.values[ state ] = getValueFromState( state )
                
            # For every predecessor state of the state being evaluated we get the difference between stored value and real value
            for predecessorState in statePredecessors[ state ]:
                diff = abs( self.values[ predecessorState ] - getValueFromState( predecessorState ) )

                if diff > self.theta:
                    priorityQueue.update( predecessorState, -diff ) 

